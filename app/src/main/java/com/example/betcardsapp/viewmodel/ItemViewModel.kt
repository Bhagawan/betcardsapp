package com.example.betcardsapp.viewmodel

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.databinding.BaseObservable
import com.example.betcardsapp.data.Bet
import com.example.betcardsapp.util.SavedPrefs
import java.text.SimpleDateFormat
import java.util.*

class ItemViewModel(val bet: Bet,private val clickListener: BetClickListener): BaseObservable() {
    var amount : String = try {
        val a = bet.amount.toInt()
        val c = bet.coefficient.toFloat()
        (a * c).toString()
    } catch (e: Exception) {
        "0"
    }

    //    var date: String = DateFormat.format("dd MMM yyyy",bet.data).toString()
//    var time: String = DateFormat.format("HH:mm",bet.data).toString()

    fun edit() {
        clickListener.editAction(bet)
    }

    fun delete(context: Context) {
        SavedPrefs.removeBet(context, bet)
        clickListener.deleteAction(bet)
    }

    fun toggleNotification(view: View) {

        if(view is SwitchCompat) {
            val dFormatter = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())
            try {
                val d = dFormatter.parse(bet.data + " " + bet.time)
                d?.let { if(it.before(Date())) throw Exception("late")}
                val oldBet = bet.copy()
                bet.notification = !bet.notification
                SavedPrefs.changeBet(view.context, oldBet, bet)
                if(view.isChecked) d?.let { clickListener.addNotification(bet) }
                else d?.let { clickListener.removeNotification(bet) }
            } catch (e: Exception) {
                val message = if(e.message == "late") "Матч уже прошел"
                else "Дата и время должны быть в читаемом виде"
                Toast.makeText(view.context,message, Toast.LENGTH_SHORT).show()
                view.isChecked = false
            }
        }
    }

}