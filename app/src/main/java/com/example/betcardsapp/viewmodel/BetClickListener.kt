package com.example.betcardsapp.viewmodel

import com.example.betcardsapp.data.Bet

interface BetClickListener {
    fun editAction(bet: Bet)
    fun deleteAction(bet: Bet)
    fun addNotification(bet: Bet)
    fun removeNotification(bet: Bet)
}