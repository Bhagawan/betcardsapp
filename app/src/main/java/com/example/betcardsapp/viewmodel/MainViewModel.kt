package com.example.betcardsapp.viewmodel

import android.content.Context
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import com.example.betcardsapp.BR
import com.example.betcardsapp.data.Bet
import com.example.betcardsapp.util.NotificationService
import com.example.betcardsapp.util.SavedPrefs

class MainViewModel: BaseObservable() {
    private var service : NotificationService? = null
    private var refreshService = true

    @Bindable
    var dataChanged = true

    val backUrl = "http://195.201.125.8/BetCardsApp/back.png"

    val new = MutableLiveData<Bet?>()
    val startService = MutableLiveData<Boolean>()

    val onBetClickListener = object :BetClickListener {
        override fun editAction(bet: Bet) {
            new.postValue(bet)
        }

        override fun deleteAction(bet: Bet) {
            service?.removeBet(bet)
        }

        override fun addNotification(bet: Bet) {
            if(service != null) service?.addBet(bet)
            else {
                startService.postValue(true)
                refreshService = true
            }
        }

        override fun removeNotification(bet: Bet) {
            service?.removeBet(bet)
        }
    }

    fun setService(service: NotificationService?, bets: List<Bet>?) {
        this.service = service
        if(refreshService && bets != null) {
            refreshService(bets)
            refreshService = false
        }
    }

    private fun refreshService(bets: List<Bet>?) {
        service?.let{
            if(bets != null) {
                it.clearList()
                for(bet in bets) {
                    if(bet.notification) it.addBet(bet)
                }
            }
        }
    }

    fun refresh() {
        notifyPropertyChanged(BR.dataChanged)
    }

    fun getData(context: Context, flag: Boolean) : List<Bet> {
        val bets = SavedPrefs.getBets(context)
        refreshService(bets)
        return bets
    }

    fun newBet() {
        new.postValue(null)
    }

    fun popupDismissed() {
        notifyPropertyChanged(BR.dataChanged)
    }

}