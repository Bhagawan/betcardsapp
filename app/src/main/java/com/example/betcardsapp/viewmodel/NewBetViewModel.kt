package com.example.betcardsapp.viewmodel

import android.content.Context
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.example.betcardsapp.data.Bet
import com.example.betcardsapp.util.SavedPrefs

class NewBetViewModel(): BaseObservable() {
    private var initBet: Bet? = null

    var name = ""
    var date = ""
    var time = ""
    var amount = ""
    var coeff = ""

    val close = MutableLiveData<Boolean>()

    constructor(bet: Bet?): this() {
        initBet = bet
        bet?.let {
            name = it.name
            date = it.data
            time = it.time
            amount = it.amount
            coeff = it.coefficient
        }
    }

    fun cancel() {
        close.postValue(false)
    }

    fun save(context: Context) {
        if(initBet != null) SavedPrefs.changeBet(context, initBet!!, Bet(name, date, time, amount, coeff, false))
        else SavedPrefs.saveBet(context, Bet(name, date, time, amount, coeff, false))
        close.postValue(true)
    }



}