package com.example.betcardsapp.adapter

interface DynamicAdapter<T> {
    fun setData(data: T)
}