package com.example.betcardsapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.betcardsapp.BR
import com.example.betcardsapp.R
import com.example.betcardsapp.data.Bet
import com.example.betcardsapp.viewmodel.BetClickListener
import com.example.betcardsapp.viewmodel.ItemViewModel

class BetAdapter(private val betClickListener: BetClickListener) : RecyclerView.Adapter<BetAdapter.ViewHolder>(), DynamicAdapter<List<Bet>> {
    private var items = emptyList<Bet>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_card, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setData(data: List<Bet>) {
        if(items.isEmpty()) {
            items = data
            notifyDataSetChanged()
        } else {
            if(items.size == data.size) {
                for(i in items.indices) {
                    if(items[i] != data[i]) {
                        items = data
                        notifyItemChanged(i)
                    }
                }
            } else {
                items = data
                notifyItemInserted(data.size)
            }
        }
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Bet) {
            val viewModel = ItemViewModel(item, object : BetClickListener {
                override fun editAction(bet: Bet) {
                    betClickListener.editAction(bet)
                }

                override fun deleteAction(bet: Bet) {
                    betClickListener.deleteAction(bet)
                    items = items.minus(bet)
                    notifyItemRemoved(adapterPosition)
                }
                override fun addNotification(bet: Bet) {
                    betClickListener.addNotification(bet)
                }
                override fun removeNotification(bet: Bet) {
                    betClickListener.removeNotification(bet)
                }
            })

            binding.setVariable(BR.viewModel, viewModel)
        }
    }
}