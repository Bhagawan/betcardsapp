package com.example.betcardsapp

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.IBinder
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import com.example.betcardsapp.data.Bet
import com.example.betcardsapp.databinding.ActivityMainBinding
import com.example.betcardsapp.databinding.ItemCardNewBinding
import com.example.betcardsapp.util.NotificationService
import com.example.betcardsapp.util.SavedPrefs
import com.example.betcardsapp.viewmodel.MainViewModel
import com.example.betcardsapp.viewmodel.NewBetViewModel
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private val viewModel = MainViewModel()
    private lateinit var target: Target
    private var serviceBind : NotificationService? = null

    private val connection = object :ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            serviceBind = (service as NotificationService.NotificationBinder).service
            viewModel.setService(serviceBind, SavedPrefs.getBets(applicationContext))
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            serviceBind = null
            viewModel.setService(serviceBind, null)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.viewModel = viewModel

        viewModel.new.observe(this) {
            newBetPopup(it)
        }

        viewModel.startService.observe(this) {
            if(it)  setupService()
        }

        setContentView(binding.root)
        loadBackground()
        checkNotifications()

    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onResume() {
        bindService(Intent(this, NotificationService::class.java), connection, 0)
        super.onResume()
    }

    override fun onPause() {
        unbindService(connection)
        super.onPause()
    }

    override fun onDestroy() {
        serviceBind?.destroy()
        super.onDestroy()
    }

    @SuppressLint("InflateParams", "ClickableViewAccessibility")
    private fun newBetPopup(bet: Bet?) {
        val popupBinding: ItemCardNewBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.item_card_new, null, false)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        val popupViewModel = NewBetViewModel(bet)
        popupViewModel.close.observe(this) {
            if(it) {
                bet?.let { bet -> serviceBind?.removeBet(bet) }
                viewModel.popupDismissed()
            }
            popupWindow.dismiss()
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true
        popupBinding.root.findFocus()

        popupWindow.setTouchInterceptor{ _, event ->
            if (event?.action == MotionEvent.ACTION_DOWN) {
                val v = popupBinding.root.findFocus()
                if (v is EditText) {
                    val outRect = Rect()
                    v.getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        v.clearFocus()
                        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                    }
                }
            }
            false
        }

        popupBinding.setVariable(BR.viewModel, popupViewModel)

        popupWindow.animationStyle = R.style.PopupBetAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { binding.root.background = BitmapDrawable(resources, it) }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }

        }
        Picasso.get().load(viewModel.backUrl).into(target)
    }

    private fun checkNotifications() {
        val bets = SavedPrefs.getBets(this)
        val dFormatter = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH)
        var setup = false
        for(bet in bets) {
            try {
                val date = dFormatter.parse(bet.data + " " + bet.time)
                date?.let {
                    if(it.before(Date()) && bet.notification) {
                        val oldBet = bet.copy()
                        bet.notification = false
                        SavedPrefs.changeBet(this, oldBet, bet)
                    }
                }
            } catch (e: Exception) { }
            if (bet.notification) setup = true
        }
        viewModel.refresh()
        if(setup) setupService()
    }

    private fun setupService() {
        val i = Intent(this, NotificationService::class.java)
        startService(i)
        bindService(i, connection, 0)
    }

}