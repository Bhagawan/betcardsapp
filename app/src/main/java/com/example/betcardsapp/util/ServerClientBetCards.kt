package com.example.betcardsapp.util

import com.example.betcardsapp.data.BetCardsSplashResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ServerClientBetCards {

    @FormUrlEncoded
    @POST("BetCardsApp/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Response<BetCardsSplashResponse>

    companion object {
        fun create() : ServerClientBetCards {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClientBetCards::class.java)
        }
    }

}