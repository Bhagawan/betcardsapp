package com.example.betcardsapp.util

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.betcardsapp.MainActivity
import com.example.betcardsapp.R
import com.example.betcardsapp.data.Bet
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*


class NotificationService: Service() {
    private val serviceScope = CoroutineScope(Dispatchers.Default)
    private val currentBets = ArrayList<Bet>()
    private var timer: Job? = null
    private val dFormatter = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH)
    private var bound = true
    private var tempJob: Job? = null

    private lateinit var manager :NotificationManager

     inner class NotificationBinder: Binder() {
         val service: NotificationService
            get() = this@NotificationService
    }

    override fun onBind(intent: Intent?): IBinder {
        bound = true
        return NotificationBinder()
    }

    override fun onUnbind(intent: Intent?): Boolean {
        bound = false
        return true
    }

    override fun onRebind(intent: Intent?) {
        bound = true
        super.onRebind(intent)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        manager  = baseContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(timer == null) startTimer()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("1", "name", NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }
        return START_STICKY
    }

    fun addBet(bet: Bet) {
        if(!currentBets.contains(bet)) {
            currentBets.add(bet)
            if(currentBets.size == 1) startTimer()
        }
    }

    fun removeBet(bet: Bet) {
        for(b in currentBets) if(bet.name == b.name && bet.data == b.data && bet.time == b.time && bet.amount == b.amount && bet.coefficient == b.coefficient) {
            tempJob = serviceScope.launch {
                currentBets.remove(b)
            }
        }
        if(currentBets.isEmpty()) {
            manager.cancel(1)
            stopSelf()
        }

    }

    fun clearList() {
        currentBets.clear()
    }

    fun destroy() {
        if(currentBets.isEmpty()) stopSelf()
        tempJob?.let { if(it.isActive) it.cancel() }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun createNotification(text: String): Notification {
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, "1")
            .setContentTitle("Скоро матч")
            .setContentText(text)
            .setSmallIcon(R.drawable.ic_baseline_notifications)
            .setAutoCancel(true)

        val resultIntent = Intent(this, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(resultPendingIntent)
        return builder.build()
    }

    private fun startTimer() {
        timer = serviceScope.async {
            while (currentBets.isNotEmpty()) {
                tempJob?.join()
                delay(1000)
                val t = dFormatter.timeZone.rawOffset
                val currTime = System.currentTimeMillis() + t
                val ddd = Date(currTime)
                var hideNotification = true
                for (bet in currentBets) {
                    val date = dFormatter.parse(bet.data + " " + bet.time)
                    if (date != null) {
                        if (date.time in ddd.time..(currTime + 3600000)) {
                            hideNotification = false
                            if(!bound) {
                                //startForeground(1, createNotification(bet.name + " : " + bet.time))
                                manager.notify(1, createNotification(bet.name + " : " + bet.time))
                            }
                        } else Log.d((date.time - currTime).toString(), "")
                    }
                }
                if(hideNotification) manager.cancel(1)
            }
            if (!bound) stopSelf()
        }
    }
}