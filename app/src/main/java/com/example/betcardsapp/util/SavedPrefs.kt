package com.example.betcardsapp.util

import android.content.Context
import com.example.betcardsapp.data.Bet
import com.google.gson.Gson

class SavedPrefs {
    companion object {
        fun saveBet(context: Context, bet: Bet) {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            val g = Gson()
            val oldBets = g.fromJson(shP.getString("bets", ""), Bets::class.java)?: Bets(emptyList())
            val newBets = Bets(oldBets.bets.plus(bet))
            shP.edit().putString("bets", g.toJson(newBets)).apply()
        }

        fun getBets(context: Context) : List<Bet> {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            return (Gson().fromJson(shP.getString("bets", ""), Bets::class.java)?: Bets(emptyList())).bets
        }

        fun changeBet(context: Context, oldBet: Bet, newBet: Bet) {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            val g = Gson()
            val savedBets = g.fromJson(shP.getString("bets", ""), Bets::class.java)?: Bets(emptyList())
            for(b in savedBets.bets) {
                if(b == oldBet) {
                    val n = savedBets.bets.indexOf(b)
                    val changedBets = savedBets.bets.toMutableList()
                    changedBets[n] = newBet
                    shP.edit().putString("bets", g.toJson(Bets(changedBets.toList()))).apply()
                    break
                }
            }
        }

        fun removeBet(context: Context, bet: Bet) {
            val shP = context.getSharedPreferences("savedBets", Context.MODE_PRIVATE)
            val g = Gson()
            val savedBets = g.fromJson(shP.getString("bets", ""), Bets::class.java)?: Bets(emptyList())
            val changedBets = Bets(savedBets.bets.minus(bet))
            shP.edit().putString("bets", g.toJson(changedBets)).apply()
        }
    }


    data class Bets(val bets: List<Bet>)
}