package com.example.betcardsapp.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.betcardsapp.adapter.BetAdapter
import com.example.betcardsapp.adapter.DynamicAdapter
import com.example.betcardsapp.viewmodel.BetClickListener
import com.squareup.picasso.Picasso
import im.delight.android.webview.AdvancedWebView

@Suppress("UNCHECKED_CAST")
@BindingAdapter("recyclerData")
fun <T> setData(recycler: RecyclerView, data: T) {
    if(recycler.adapter != null){
        if(recycler.adapter is DynamicAdapter<*>) (recycler.adapter as DynamicAdapter<T>).setData(data)
    }
}

@BindingAdapter("imageUrl")
fun loadUrl(view: ImageView, url: String) {
     Picasso.get().load(url).fit().into(view)
}

@BindingAdapter("recyclerAdapter")
fun initAdapter(recycler: RecyclerView, clickListener: BetClickListener) {
    if(recycler.adapter == null) {
        recycler.layoutManager = LinearLayoutManager(recycler.context)
        recycler.adapter = BetAdapter(clickListener)
    }
}

@BindingAdapter("url")
fun setUrl(v: AdvancedWebView, url: String) {
    v.loadUrl(url)
}
