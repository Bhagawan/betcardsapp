package com.example.betcardsapp.data

import androidx.annotation.Keep

@Keep
class BetCardsSplashResponse(val url : String)