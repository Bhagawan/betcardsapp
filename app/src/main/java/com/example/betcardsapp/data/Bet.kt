package com.example.betcardsapp.data

import androidx.annotation.Keep

@Keep
data class Bet(val name: String, val data: String, val time: String, val amount: String, val coefficient: String , var notification :Boolean) {

    override fun equals(other: Any?): Boolean {
        return if(other is Bet) name == other.name && data == other.data && time == other.time && amount == other.amount && coefficient == other.coefficient && notification == other.notification
        else false
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + data.hashCode()
        result = 31 * result + time.hashCode()
        result = 31 * result + amount.hashCode()
        result = 31 * result + coefficient.hashCode()
        result = 31 * result + notification.hashCode()
        return result
    }
}
